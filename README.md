[ ![Codeship Status for chrisdostert/t-mobile-eui-persistence](https://codeship.com/projects/a911fc60-810c-0133-8192-7e50579f0890/status?branch=master)](https://codeship.com/projects/121127)

# CI

### Flow
Checkins continuously (upon checkin) built & deployed via [CodeShip](https://codeship.com/projects/121127) to [EC2](http://ec2-52-90-111-120.compute-1.amazonaws.com/sessions/)

### sample commands

create session
```bash
curl -H "Content-Type: application/json" -X POST -d '{"key1":"value1","sessionId":"1"}' http://ec2-52-90-111-120.compute-1.amazonaws.com/sessions/1
```

fetch session
```bash
curl -H "Content-Type: application/json" http://ec2-52-90-111-120.compute-1.amazonaws.com/sessions/1

```

# Development

### Required Software
- NodeJs
- NPM

### Scripts

Install:
```bash
npm install
```

Run:
```bash
npm start
```
