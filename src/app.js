var express = require('express');
var persistenceManager = require('./persistenceManager');
var bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.json());

app.post('/sessions', (req, res)=> {

        persistenceManager
            .persistUserSession(req.body)
            .then(res.sendStatus(201))
            .catch(errorData => res.status(500).send(errorData));

    }
);

app.get('/sessions/:sessionId', (req, res) => {

        persistenceManager
            .fetchIamSessionId(req.params.sessionId)
            .then(sessionData => res.send(sessionData))
            .catch(errorData => res.status(500).send(errorData));

    }
);

var server = app.listen(3000, function () {
        var host = server.address().address;
        var port = server.address().port;

        console.log('t-mobile-eui-persistence listening at http://%s:%s', host, port);
    }
);
