var util = require('util'),
    Memcached = require('memcached'),
    memcached = new Memcached(
        [
            't-mobile-eui.jj9lmw.0001.use1.cache.amazonaws.com:11211',
            't-mobile-eui.jj9lmw.0002.use1.cache.amazonaws.com:11211'
        ]
    ),
    oneDayInSeconds = 60 * 60 * 24,
    objectLifetimeInSeconds = oneDayInSeconds;

// add some server connectivity logging
memcached.on(
    'failure',
    details => {
        util.error( "Server " + details.server + "went down due to: " + details.messages.join( '' ) )
    }
);
memcached.on(
    'reconnecting',
    details => {
        util.debug( "Total downtime caused by server " + details.server + " :" + details.totalDownTime + "ms")
    }
);

module.exports = {
    saveClientSessionId: saveClientSessionId,
    saveIamSessionId: saveIamSessionId,
    fetchIamSessionId: fetchIamSessionId,
    deleteUserSession: deleteUserSession,
    persistUserSession: persistUserSession
};

function saveClientSessionId(sessionId) {

    // this appears to be completely unnecessary.
    return new Promise(
        (resolve, reject) => {
            memcached.set(
                sessionId,
                JSON.stringify({}),
                objectLifetimeInSeconds,
                (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                }
            );

        }
    );

}

function saveIamSessionId(sessionInfo) {

    return new Promise(
        (resolve, reject) => {

            memcached.set(
                sessionInfo.sessionId,
                JSON.stringify({iamSessionId: sessionInfo.iamSessionId}),
                objectLifetimeInSeconds,
                err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                }
            );

        }
    );

}

function fetchIamSessionId(sessionId) {

    return new Promise(
        (resolve, reject) => {

            memcached.get(
                sessionId,
                (err, data) => {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(JSON.parse(data));
                    }
                }
            );

        }
    );

}

function deleteUserSession(sessionId) {

    return new Promise(
        (resolve, reject) => {

            memcached.del(
                sessionId,
                err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                }
            );

        }
    );

}

function persistUserSession(sessionData) {

    return new Promise(
        (resolve, reject) => {
            memcached.set(
                sessionData.sessionId,
                JSON.stringify(sessionData),
                objectLifetimeInSeconds,
                err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                }
            );

        }
    );

}

